# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the entangle package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: entangle\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-16 23:09+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/frontend/entangle-preferences-display.ui:784
msgid "0"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1322
msgid "1.15:1 - Movietone"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1325
msgid "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1328
msgid "1.37:1 - 35mm movie"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1330
msgid "1.44:1 - IMAX"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1333
msgid "1.50:1 (3:2, 15:10)- 35mm SLR"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1339
msgid "1.66:1 (5:3, 15:9) - Super 16mm"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1336
msgid "1.6:1 (8:5, 16:10) - Widescreen"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1342
msgid "1.75:1 (7:4) - Widescreen"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1345
msgid "1.77:1 (16:9) - APS-H / HDTV / Widescreen"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1347
msgid "1.85:1 - 35mm Widescreen"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1387
msgid "12.00:1 - Circle-Vision 360"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1320
msgid "1:1 - Square / MF 6x6"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1350
msgid "2.00:1 - SuperScope"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1353
msgid "2.10:1 (21:10) - Planned HDTV"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1356
msgid "2.20:1 (11:5, 22:10) - 70mm movie"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1358
msgid "2.35:1 - CinemaScope"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1362
msgid "2.37:1 (64:27)- HDTV cinema"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1365
msgid "2.39:1 (12:5)- Panavision"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1368
msgid "2.55:1 (23:9)- CinemaScope 55"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1370
msgid "2.59:1 (13:5)- Cinerama"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1374
msgid "2.66:1 (8:3, 24:9)- Super 16mm"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1377
msgid "2.76:1 (11:4) - Ultra Panavision"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1379
msgid "2.93:1 - MGM Camera 65"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:886
msgid "3"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1382
msgid "3:1 APS Panorama"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1384
msgid "4.00:1 - Polyvision"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:101
msgid "<b>Capture</b>"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:278
msgid "<b>Colour management</b>"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:687
msgid "<b>Image Viewer</b>"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:558
msgid "<b>Interface</b>"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:488
msgid "<b>Plugins</b>"
msgstr ""

#: src/frontend/entangle-help-about.ui:8
msgid "About - Entangle"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:1040
msgid "Absolute colourimetric"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1299
msgid "All files (*.*)"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:729
msgid "Apply mask to alter aspect ratio"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:745
msgid "Aspect ratio:"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:139
msgctxt "shortcut window"
msgid "Autofocus"
msgstr ""

#: src/backend/entangle-camera.c:2698
#, c-format
msgid "Autofocus control not available with this camera"
msgstr ""

#: src/backend/entangle-camera.c:2710
#, c-format
msgid "Autofocus control was not a toggle widget"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1832
msgid "Autofocus failed"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:600
msgid "Automatically connect to cameras at startup"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:214
msgid "Automatically synchronize camera clock"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2853
msgid "Automation"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:902
msgid "Background:"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:87
msgid "Best Fit"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:614
msgid "Blank screen when capturing images"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1168
msgid "Camera connect failed"
msgstr ""

#: src/frontend/entangle-control-panel.c:86
msgid "Camera control update failed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1131
msgid "Camera is in use"
msgstr ""

#: src/frontend/entangle-camera-manager.c:801
#: src/frontend/entangle-camera-manager.c:1056
msgid "Camera load controls failed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1104
#: src/frontend/entangle-camera-manager.c:2226
msgid "Cancel"
msgstr ""

#: src/backend/entangle-camera.c:1035
#, c-format
msgid "Cannot capture image while not opened"
msgstr ""

#: src/backend/entangle-camera.c:1448
#, c-format
msgid "Cannot delete file while not opened"
msgstr ""

#: src/backend/entangle-camera.c:1302
#, c-format
msgid "Cannot download file while not opened"
msgstr ""

#: src/backend/entangle-camera-list.c:221 src/backend/entangle-camera.c:647
#, c-format
msgid "Cannot initialize gphoto2 abilities"
msgstr ""

#: src/backend/entangle-camera-list.c:380 src/backend/entangle-camera.c:659
#, c-format
msgid "Cannot initialize gphoto2 ports"
msgstr ""

#: src/backend/entangle-camera-list.c:224 src/backend/entangle-camera.c:653
#, c-format
msgid "Cannot load gphoto2 abilities"
msgstr ""

#: src/backend/entangle-camera-list.c:383 src/backend/entangle-camera.c:665
#, c-format
msgid "Cannot load gphoto2 ports"
msgstr ""

#: src/backend/entangle-camera.c:1156
#, c-format
msgid "Cannot preview image while not opened"
msgstr ""

#: src/backend/entangle-camera.c:1577
#, c-format
msgid "Cannot wait for events while not opened"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1610
#: src/frontend/entangle-camera-picker.c:518
#: src/frontend/entangle-preferences-display.c:1261
#: src/frontend/entangle-preferences-display.ui:252
msgid "Capture"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:119
msgctxt "shortcut window"
msgid "Capture a single image"
msgstr ""

#: src/frontend/entangle-camera-manager.c:611
#: src/frontend/entangle-camera-manager.c:615
msgid "Capture an image"
msgstr ""

#: src/backend/entangle-camera.c:3227
#, c-format
msgid "Capture target setting not available with this camera"
msgstr ""

#: src/org.entangle_photo.Manager.desktop:12
msgid "Capture;Camera;Tethered;Photo;"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1403
msgid "Center lines"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1099
#: src/frontend/entangle-camera-manager.c:2221
msgid ""
"Check that the camera is not\n"
"\n"
" - opened by another photo <b>application</b>\n"
" - mounted as a <b>filesystem</b> on the desktop\n"
" - in <b>sleep mode</b> to save battery power\n"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:24
msgctxt "shortcut window"
msgid "Close all windows & exit"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1265
#: src/frontend/entangle-preferences-display.ui:461
msgid "Color Management"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:432
msgid "Colour managed display"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:166
msgid "Continue preview mode after capture"
msgstr ""

#: src/frontend/entangle-camera-manager.c:613
#: src/frontend/entangle-camera-manager.c:617
msgid "Continuous capture preview"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:13
msgctxt "shortcut window"
msgid "Controlling the application"
msgstr ""

#: src/backend/entangle-camera.c:2467 src/backend/entangle-camera.c:2520
#: src/backend/entangle-camera.c:2691 src/backend/entangle-camera.c:2846
#: src/backend/entangle-camera.c:3060 src/backend/entangle-camera.c:3220
#, c-format
msgid "Controls not available for this camera"
msgstr ""

#: src/backend/entangle-camera.c:2461 src/backend/entangle-camera.c:2514
#: src/backend/entangle-camera.c:2685 src/backend/entangle-camera.c:2840
#: src/backend/entangle-camera.c:3054 src/backend/entangle-camera.c:3214
#, c-format
msgid "Controls not available when camera is closeed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2348
#: src/frontend/entangle-camera-manager.c:2469
msgid "Delete"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:198
msgid "Delete file from camera after downloading"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:367
msgid "Detect the system monitor profile"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:797
msgid "Display focus point during preview"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:31
msgctxt "shortcut window"
msgid "Display help manual"
msgstr ""

#: src/org.entangle_photo.Manager.metainfo.xml:7
#: src/org.entangle_photo.Manager.desktop:4
msgid "Entangle"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:8
msgid "Entangle Preferences"
msgstr ""

#: src/org.entangle_photo.Manager.metainfo.xml:14
msgid ""
"Entangle can trigger the camera shutter to capture new images. When "
"supported by the camera, a continuously updating preview of the scene can be "
"displayed prior to capture. Images will be downloaded and displayed as they "
"are captured by the camera. Entangle also allows the settings of the camera "
"to be changed from the computer."
msgstr ""

#: src/org.entangle_photo.Manager.metainfo.xml:10
msgid ""
"Entangle is a program used to control digital cameras that are connected to "
"the computer via USB."
msgstr ""

#: src/org.entangle_photo.Manager.metainfo.xml:22
msgid ""
"Entangle is compatible with most DSLR cameras from Nikon and Canon, some of "
"their compact camera models, and a variety of cameras from other "
"manufacturers."
msgstr ""

#: src/frontend/entangle-camera-manager.c:1834
msgid "Entangle: Camera autofocus failed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1170
msgid "Entangle: Camera connect failed"
msgstr ""

#: src/frontend/entangle-control-panel.c:88
msgid "Entangle: Camera control update failed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:803
#: src/frontend/entangle-camera-manager.c:1058
msgid "Entangle: Camera load controls failed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1861
msgid "Entangle: Camera manual focus failed"
msgstr ""

#: src/frontend/entangle-camera-manager.c:775
msgid "Entangle: Operation failed"
msgstr ""

#: src/backend/entangle-camera.c:2901 src/backend/entangle-camera.c:2935
#, c-format
msgid "Failed to read manual focus choice %d: %s %d"
msgstr ""

#: src/backend/entangle-camera.c:2717 src/backend/entangle-camera.c:2725
#, c-format
msgid "Failed to set autofocus state: %s %d"
msgstr ""

#: src/backend/entangle-camera.c:3248
#, c-format
msgid "Failed to set capture target: %s %d"
msgstr ""

#: src/backend/entangle-camera.c:2884 src/backend/entangle-camera.c:2908
#: src/backend/entangle-camera.c:2926 src/backend/entangle-camera.c:2942
#, c-format
msgid "Failed to set manual focus state: %s %d"
msgstr ""

#: src/backend/entangle-camera.c:3086
#, c-format
msgid "Failed to set time state: %s %d"
msgstr ""

#: src/backend/entangle-camera.c:2556 src/backend/entangle-camera.c:2563
#, c-format
msgid "Failed to set viewfinder state: %s %d"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:145
msgid "Filename pattern:"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:135
msgctxt "shortcut window"
msgid "Focus control (during live preview)"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:160
msgctxt "shortcut window"
msgid "Focus in (large step)"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:146
msgctxt "shortcut window"
msgid "Focus in (small step)"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:167
msgctxt "shortcut window"
msgid "Focus out (large step)"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:153
msgctxt "shortcut window"
msgid "Focus out (small step)"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:99
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1415
msgid "Golden sections"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:815
msgid "Grid lines:"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:952
msgid "Highlight areas of overexposure"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:928
msgid "Highlight:"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1294
msgid "ICC profiles (*.icc, *.icm)"
msgstr ""

#: src/frontend/entangle-camera-picker.ui:198
msgid "IP Address:"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1255
#: src/frontend/entangle-preferences-display.ui:982
msgid "Image Viewer"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:40
msgctxt "shortcut window"
msgid "Image display"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2851
msgid "Image histogram"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1250
#: src/frontend/entangle-preferences-display.ui:660
msgid "Interface"
msgstr ""

#: src/backend/entangle-camera.c:2853
#, c-format
msgid "Manual focus control not available with this camera"
msgstr ""

#: src/backend/entangle-camera.c:2865
#, c-format
msgid "Manual focus control was not a range or radio widget"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1859
msgid "Manual focus failed"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:757
msgid "Mask opacity:"
msgstr ""

#: src/frontend/entangle-script-simple.c:61
msgid "Missing 'execute' method implementation"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:325
msgid "Mode of operation:"
msgstr ""

#: src/frontend/entangle-camera-picker.c:515
msgid "Model"
msgstr ""

#: src/frontend/entangle-camera-manager.c:835
msgid "Monitor"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:345
msgid "Monitor profile:"
msgstr ""

#: src/frontend/entangle-camera-picker.ui:226
msgid "Network camera"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1139
#: src/frontend/entangle-camera-picker.c:134
msgid "No"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1018
#: src/frontend/entangle-camera-manager.c:1019
#: src/frontend/entangle-control-panel.c:835
msgid "No camera connected"
msgstr ""

#: src/frontend/entangle-camera-picker.ui:157
msgid ""
"No cameras were detected, check that\n"
"\n"
"  - the <b>cables</b> are connected\n"
"  - the camera is <b>turned on</b>\n"
"  - the camera is in the <b>correct mode</b>\n"
"  - the camera is a <b>supported</b> model\n"
"\n"
"USB cameras are automatically detected\n"
"when plugged in, for others try a refresh\n"
"or enter a network camera IP address"
msgstr ""

#: src/frontend/entangle-script-config.c:159
msgid "No config options"
msgstr ""

#: src/frontend/entangle-control-panel.c:843
msgid "No controls available"
msgstr ""

#: src/frontend/entangle-script-config.c:76
msgid "No script"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1401
msgid "None"
msgstr ""

#: src/frontend/entangle-control-panel.c:309
msgid "Off"
msgstr ""

#: src/frontend/entangle-control-panel.c:309
msgid "On"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2458
msgid "Open"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:17
msgctxt "shortcut window"
msgid "Open a new camera window"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2463
msgid "Open with"
msgstr ""

#: src/frontend/entangle-camera-manager.c:774
#, c-format
msgid "Operation: %s"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:855
msgid "Overlay earlier images"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:873
msgid "Overlay layers:"
msgstr ""

#: src/frontend/entangle-preferences-display.c:951
msgid "Pattern must contain 'XXXXXX' placeholder"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:1031
msgid "Perceptual"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1272
#: src/frontend/entangle-preferences-display.ui:531
msgid "Plugins"
msgstr ""

#: src/frontend/entangle-camera-picker.c:516
msgid "Port"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:106
msgctxt "shortcut window"
msgid "Presentation"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1691
msgid "Preview"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1409
msgid "Quarters"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:333
msgid "RGB profile:"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:1034
msgid "Relative colourimetric"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:357
msgid "Rendering intent:"
msgstr ""

#: src/frontend/entangle-control-panel.c:769
msgid "Reset controls"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1106
#: src/frontend/entangle-camera-manager.c:2227
msgid "Retry"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1406
msgid "Rule of 3rds"
msgstr ""

#: src/frontend/entangle-preferences-display.c:1411
msgid "Rule of 5ths"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:1037
msgid "Saturation"
msgstr ""

#: src/frontend/entangle-dpms.c:57
msgid "Screen blanking is not available on this display"
msgstr ""

#: src/frontend/entangle-dpms.c:69
msgid "Screen blanking is not implemented on this platform"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1629
msgid "Script"
msgstr ""

#: src/frontend/entangle-camera-picker.ui:97
msgid "Select a camera to connect to:"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1463
msgid "Select a folder"
msgstr ""

#: src/frontend/entangle-camera-manager.c:2502
msgid "Select application..."
msgstr ""

#: src/frontend/entangle-camera-picker.ui:8
msgid "Select camera - Entangle"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1793
msgid "Set clock"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:630
msgid "Show linear histogram"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:115
msgctxt "shortcut window"
msgid "Shutter control"
msgstr ""

#: src/frontend/entangle-help-about.ui:17
#: src/org.entangle_photo.Manager.desktop:5
msgid "Tethered Camera Control & Capture"
msgstr ""

#: src/org.entangle_photo.Manager.metainfo.xml:8
msgid "Tethered Camera Control &amp; Capture"
msgstr ""

#: src/org.entangle_photo.Manager.metainfo.xml:39
msgid "The Entangle Photo project"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1135
msgid ""
"The camera cannot be opened because it is currently mounted as a filesystem. "
"Do you wish to umount it now ?"
msgstr ""

#: src/frontend/entangle-camera-manager.c:623
#: src/frontend/entangle-camera-manager.c:626
msgid "This camera does not support image capture"
msgstr ""

#: src/frontend/entangle-camera-manager.c:632
#: src/frontend/entangle-camera-manager.c:635
msgid "This camera does not support image preview"
msgstr ""

#: src/backend/entangle-camera.c:3067
#, c-format
msgid "Time setting not available with this camera"
msgstr ""

#: src/backend/entangle-camera.c:3239
#, c-format
msgid "Time setting was not a choice widget"
msgstr ""

#: src/backend/entangle-camera.c:3079
#, c-format
msgid "Time setting was not a date widget"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:86
msgctxt "shortcut window"
msgid "Toggle histogram scale"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:72
msgctxt "shortcut window"
msgid "Toggle image mask"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:126
msgctxt "shortcut window"
msgid "Toggle live preview"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:79
msgctxt "shortcut window"
msgid "Toggle overexposure highlighting"
msgstr ""

#: src/backend/entangle-camera.c:1045
#, c-format
msgid "Unable to capture image: %s"
msgstr ""

#: src/backend/entangle-camera.c:1169
#, c-format
msgid "Unable to capture preview: %s"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1094
#: src/frontend/entangle-camera-manager.c:2215
#, c-format
msgid "Unable to connect to camera: %s"
msgstr ""

#: src/backend/entangle-camera.c:1464
#, c-format
msgid "Unable to delete file: %s"
msgstr ""

#: src/backend/entangle-camera.c:2215
#, c-format
msgid "Unable to fetch camera control configuration: %s"
msgstr ""

#: src/backend/entangle-camera.c:1797 src/backend/entangle-camera.c:1956
#: src/backend/entangle-camera.c:2095
#, c-format
msgid "Unable to fetch widget name"
msgstr ""

#: src/backend/entangle-camera.c:1791 src/backend/entangle-camera.c:1950
#: src/backend/entangle-camera.c:2089 src/backend/entangle-camera.c:2536
#: src/backend/entangle-camera.c:2704 src/backend/entangle-camera.c:2859
#: src/backend/entangle-camera.c:3073 src/backend/entangle-camera.c:3233
#, c-format
msgid "Unable to fetch widget type"
msgstr ""

#: src/backend/entangle-camera.c:1322
#, c-format
msgid "Unable to get camera file: %s"
msgstr ""

#: src/backend/entangle-camera.c:1175 src/backend/entangle-camera.c:1329
#, c-format
msgid "Unable to get file data: %s"
msgstr ""

#: src/backend/entangle-camera.c:1181
#, c-format
msgid "Unable to get filename: %s"
msgstr ""

#: src/backend/entangle-camera.c:692
#, c-format
msgid "Unable to initialize camera: %s"
msgstr ""

#: src/backend/entangle-camera.c:2206
#, c-format
msgid "Unable to load controls, camera is not opened"
msgstr ""

#: src/backend/entangle-camera.c:2361 src/backend/entangle-camera.c:2571
#: src/backend/entangle-camera.c:2733 src/backend/entangle-camera.c:2917
#: src/backend/entangle-camera.c:3094 src/backend/entangle-camera.c:3256
#, c-format
msgid "Unable to save camera control configuration: %s"
msgstr ""

#: src/backend/entangle-camera.c:2342
#, c-format
msgid "Unable to save controls, camera is not configurable"
msgstr ""

#: src/backend/entangle-camera.c:2336
#, c-format
msgid "Unable to save controls, camera is not opened"
msgstr ""

#: src/backend/entangle-camera.c:1603
#, c-format
msgid "Unable to wait for events: %s"
msgstr ""

#: src/frontend/entangle-script.c:95
msgid "Untitled script"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:839
msgid "Use embedded preview from raw files"
msgstr ""

#: src/frontend/entangle-preferences-display.ui:182
msgid "Use preview output as capture image"
msgstr ""

#: src/backend/entangle-camera.c:2530
#, c-format
msgid "Viewfinder control not available with this camera"
msgstr ""

#: src/backend/entangle-camera.c:2542
#, c-format
msgid "Viewfinder control was not a toggle widget"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:95
msgctxt "shortcut window"
msgid "Window display"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1140
#: src/frontend/entangle-camera-picker.c:134
msgid "Yes"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:97
msgid "Zoom _In"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:101
msgid "Zoom _Out"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:65
msgctxt "shortcut window"
msgid "Zoom best"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:44
msgctxt "shortcut window"
msgid "Zoom in"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:58
msgctxt "shortcut window"
msgid "Zoom normal"
msgstr ""

#: src/frontend/entangle-help-overlay.ui:51
msgctxt "shortcut window"
msgid "Zoom out"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:46
msgid "_About"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1464
msgid "_Cancel"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:22
msgid "_Connect…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:26
msgid "_Disconnect…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:62
msgid "_Fullscreen"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:32
msgid "_Help"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:38
msgid "_Keyboard shortcuts"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:42
msgid "_Manual"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:6
msgid "_New window"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:91
msgid "_Normal Size"
msgstr ""

#: src/frontend/entangle-camera-manager.c:1465
msgid "_Open"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:10
msgid "_Preferences…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:66
msgid "_Presentation"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:53
msgid "_Quit"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:16
msgid "_Select session…"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:72
msgid "_Settings"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:34
msgid "_Supported cameras"
msgstr ""

#: src/frontend/entangle-camera-manager-menus.ui:78
msgid "_Synchronize capture"
msgstr ""

#: src/frontend/entangle-camera-support.c:184
msgid "capture"
msgstr ""

#: src/frontend/entangle-camera-support.ui:65
msgid "label"
msgstr ""

#: src/frontend/entangle-camera-support.c:190
msgid "preview"
msgstr ""

#: src/frontend/entangle-camera-support.c:196
msgid "settings"
msgstr ""
