Entangle Translations
=====================

.. image:: https://translate.fedoraproject.org/widgets/entangle/-/master/multi-auto.svg
     :target: https://translate.fedoraproject.org/engage/entangle/
     :alt: Translation status

The translations for Entangle are managed via the Weblate
tool. The Entangle application is outsourced to the Fedora
translation team in Weblate.

https://translate.fedoraproject.org/projects/entangle/master

If you are interested in doing translations of Entangle, we
request that you join the Fedora team in Weblate & submit
them that way. We cannot accept translations done outside of
Weblate, because we do not wish to have multiple translators
stepping on each others toes.
